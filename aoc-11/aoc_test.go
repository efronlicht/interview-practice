package aoc11

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//Fuel cell at  122,79, grid serial number 57: power level -5.
//Fuel cell at 217,196, grid serial number 39: power level  0.
//Fuel cell at 101,153, grid serial number 71: power level  4.

func Test_power(t *testing.T) {
	type test struct {
		x, y, serial int
		want         int8
	}
	for _, tt := range []test{
		{x: 122, y: 79, serial: 57, want: -5},
		{x: 217, y: 196, serial: 39, want: 0},
		{x: 101, y: 153, serial: 71, want: 4},
	} {
		assert.Equal(t, powerLevel(tt.x, tt.y, tt.serial), tt.want)
	}

}

func Test_PartA(t *testing.T) {
	type output struct {
		x, y int
	}
	type test struct {
		serial int
		want   output
	}
	for _, tt := range []test{
		{serial: 18, want: output{x: 33, y: 45}},
		{serial: 42, want: output{x: 21, y: 61}},
	} {
		x, y, _ := partA(tt.serial)

		assert.Equal(t, tt.want, output{x, y})

	}
}

//For grid serial number 18, the largest total square (with a total power of 113) is 16x16 and has a top-left corner of 90,269, so its identifier is 90,269,16.
//For grid serial number 42, the largest total square (with a total power of 119) is 12x12 and has a top-left corner of 232,251, so its identifier is 232,251,12.
func Test_PartB(t *testing.T) {
	type output struct {
		x, y, size int
	}
	type test struct {
		serial int
		want   output
	}
	for _, tt := range []test{
		{serial: 18, want: output{x: 90, y: 269, size: 16}},
		{serial: 42, want: output{x: 232, y: 251, size: 12}},
	} {
		x, y, size := partB(tt.serial)
		assert.Equal(t, tt.want, output{x, y, size})

	}

}
