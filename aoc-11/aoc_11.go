package aoc11

const SERIAL = 6392

// Find the fuel cell's rack ID, which is its X coordinate plus 10.
// Begin with a power level of the rack ID times the Y coordinate.
// Increase the power level by the value of the grid serial number (your puzzle input).
// Set the power level to itself multiplied by the rack ID.
// Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
// Subtract 5 from the power level.

func powerLevel(x, y, serial int) int8 {
	rackID := x + 10
	power := rackID*y + serial
	power *= rackID
	power = ((power % 1000) / 100) % 10
	power -= 5
	return int8(power)
}

type row = [300]int8
type grid = [300]row

// find the upper left corner of the 3x3 square with the largest total power
func partA(serial int) (maxX, maxY, maxSum int) {
	var grid [300][300]int8

	for x := 0; x < 300; x++ {
		for y := 0; y < 300; y++ {
			grid[x][y] = powerLevel(x, y, serial)
		}
	}
	for x := 0; x < 300-3; x++ {
		for y := 0; y < 300-3; y++ {
			var sum int
			for dx := 0; dx < 3; dx++ {
				for dy := 0; dy < 3; dy++ {
					sum += int(grid[x+dx][y+dy])
				}
			}
			if sum > maxSum {
				maxX, maxY, maxSum = x, y, sum
			}
		}
	}
	return maxX, maxY, maxSum
}

// Realizing this, you now must find the square of any size with the largest total power.
// Identify this square by including its size as a third parameter after the top-left coordinate:
// a 9x9 square with a top-left corner of 3,5 is identified as 3,5,9.
func partB(serial int) (maxX, maxY, maxSize int) {
	var grid [300][300]int8

	for x := 0; x < 300; x++ {
		for y := 0; y < 300; y++ {
			grid[x][y] = powerLevel(x, y, serial)
		}
	}

	maxX, maxY, maxSum := partA(serial)
	maxSize = 1
	var prev [300][300]int

	edges := func(x, y, size int) (edges int) {
		var botEdge, rightEdge int
		for d := 0; d < size-1; d++ {
			botEdge += int(grid[x+d][y+size-1])
			rightEdge += int(grid[x+size-1][y+d])
		}
		return botEdge + rightEdge

	}

	for size := 1; size <= 300; size++ {
		for x := 0; x <= 300-size; x++ {
			for y := 0; y <= 300-size; y++ {
				corner := int(grid[x+size-1][y+size-1])
				sum := prev[x][y] + edges(x, y, size) + corner
				prev[x][y] = sum
				if sum > maxSum {
					maxX, maxY, maxSize, maxSum = x, y, size, sum
				}
			}
		}
	}
	return maxX, maxY, maxSize

}
