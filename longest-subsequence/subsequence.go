package subsequence

// LongestCommonSubsequence finds the length of the longest common subsequence, in _bytes_.
func LongestCommonSubsequence(a, b string) int {
	type key struct{ m, n int }

	cache := map[key]int{key{0, 0}: 0}
	var lcs func(m, n int) int
	// have to declare it first to make it available
	// within the inner scope; yes, this is weird
	lcs = func(m, n int) int {
		if m < 0 || n < 0 {
			return 0
		}
		if got, ok := cache[key{m, n}]; ok {
			return got
		}

		if a[m] == b[n] {
			cache[key{m, n}] = lcs(m-1, n-1) + 1
		} else if c, d := lcs(m-1, n), lcs(m, n-1); c > d {
			cache[key{m, n}] = c
		} else {
			cache[key{m, n}] = d
		}

		return cache[key{m, n}]
	}
	return lcs(len(a)-1, len(b)-1)
}

// FirstLongestCommonSubsequence finds the first subsequence
// (counting from the leftmost character in the first argument, that is
// part of a longest common subsequence with b)
func FirstLongestCommonSubsequence(a, b string) string {
	n := LongestCommonSubsequence(a, b)
	if n == 0 {
		return ""
	}
	for i := 0; i <= len(a)-n; i++ {
		first := a[i : i+n]
		for j := 0; j < +len(b)-n; j++ {
			if first == b[j:j+n] {
				return first
			}
		}
	}
	panic("this shouldn't be reachable")
}
