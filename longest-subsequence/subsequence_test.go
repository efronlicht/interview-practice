package subsequence

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_LongestCommonSubsequence(t *testing.T) {
	for _, tt := range []struct {
		a, b string
		want int
		msg  string
	}{
		{a: "aaaaaa" + "foobar" + "bbbbbb", b: "nnnnnnn" + "foobar" + "xxxxxx", want: len("foobar"), msg: "base case: shared substring"},
		{a: "", b: "ajlskdjaklsdjs", want: 0, msg: "trivial case is empty"},
		{a: "aob" + "m", b: "om" + "p9xxbs", want: len("om"), msg: "subSEQUENCE, not string 1"},
		{a: "foo" + "a" + "bar", b: "aaafoobnar", want: len("foobar"), msg: "subSEQUENCE, not string 2"},
	} {
		assert.Equal(t, tt.want, LongestCommonSubsequence(tt.a, tt.b), tt.msg)
	}

}

func Test_FirstLongestCommonSubsequence(t *testing.T) {

	for _, tt := range []struct{ a, b, want string }{
		{a: "aaaaaa" + "foobar" + "bbbbbb", b: "nnnnnnn" + "foobar" + "xxxxxx", want: "foobar"},
		{a: "", b: "ajlskdjaklsdjs", want: ""},
		{a: "aaaaaa", b: "bbbbb", want: ""},
		{a: "ab" + "om", b: "om" + "p9xx" + "ab", want: "om"},
	} {
		assert.Equal(t, tt.want, FirstLongestCommonSubsequence(tt.a, tt.b))
	}
}
