package subsequence

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_MaximumSumOfIncreasingSubSequence(t *testing.T) {
	a := []int{8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11}
	assert.Equal(t, 8+12+14, MaximumSumOfIncreasingSubSequence(a))

}

func Test_MaximumLengthOfIncreasingSubSequence(t *testing.T) {
	a := []int{0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15}
	assert.Equal(t, 5, MaximumLengthOfIncreasingSubSequence(a))

}

func Test_EditDistance(t *testing.T) {
	for _, tt := range []struct {
		a, b string
		want int
	}{
		{"foo", "boo", 1},
		{"foon", "fooma", 2},
		{"", "", 0},
		{"ZZzz", "zz", 2},
		{"abc", "", 3},
	} {
		assert.Equal(t, tt.want, EditDistance(tt.a, tt.b))
	}

}
