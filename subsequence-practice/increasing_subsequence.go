package subsequence

// MaximumSumOfIncreasingSubSequence finds the largest sum of a strictly monotonic increasing subsequence.
func MaximumSumOfIncreasingSubSequence(a []int) int {
	var maxSubSeqSum func(i, prev, sum int) int
	maxSubSeqSum = func(i, prev, sum int) int {

		if i == len(a) {
			return sum
		}
		exclude := maxSubSeqSum(i+1, prev, sum)
		if a[i] > prev {
			if include := maxSubSeqSum(i+1, a[i], sum+a[i]); include > exclude {
				return include
			}
		}
		return exclude
	}
	return maxSubSeqSum(0, 0, 0)
}

// MaximumLengthOfIncreasingSubSequence finds the maximum length of a strictly monotonic increasing subsequence.
func MaximumLengthOfIncreasingSubSequence(a []int) int {
	var maxSubSeqLen func(i, prev int) int
	maxSubSeqLen = func(i, prev int) int {

		if i == len(a) {
			return 0
		}
		exclude := maxSubSeqLen(i+1, prev)
		if a[i] > prev {
			if include := 1 + maxSubSeqLen(i+1, a[i]); include > exclude {
				return include
			}
		}
		return exclude
	}
	return maxSubSeqLen(0, 0)
}

// EditDistance is the Levenshtein Distance between two strings (that is, the number of deletions, subtitutions, and/or insertions to turn one string into the other)
func EditDistance(a, b string) int {
	var dist func(m, n int) int

	dist = func(m, n int) int {
		if m == 0 {
			return n
		}
		if n == 0 {
			return m
		}
		cost := 0
		if a[m-1] != b[n-1] {
			cost = 1
		}
		delete := dist(m-1, n) + 1
		insert := dist(m, n-1) + 1
		sub := dist(m-1, n-1) + cost
		if delete < insert && delete < sub {
			return delete
		} else if insert < sub {
			return insert
		} else {
			return sub
		}
	}

	return dist(len(a), len(b))

}
