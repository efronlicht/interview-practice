package hashtable

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_concurrent_hashmap(t *testing.T) {
	h := NewConcurrentStringHashMap()

	assert.Equal(t, h.Len(), 0)

	wg := new(sync.WaitGroup)
	wg.Add(26)
	for b := byte('a'); b <= byte('z'); b++ {
		go func(b byte) {
			// try and have a race condition here
			defer wg.Done()

			s := string(b)
			assert.False(t, h.Contains(s))

			h.Insert(string(b), []string{s, s})
			assert.True(t, h.Contains(s))
		}(b)
	}
	wg.Wait()
	assert.Equal(t, h.Len(), 26)
	// replacement does not change length
	assert.Equal(t, h.Insert(string('c'), []string{"foo", "bar"}), []string{"c", "c"})
	assert.Equal(t, h.Len(), 26)

	got, ok := h.Get("c")

	assert.Equal(t, got, []string{"foo", "bar"})
	assert.True(t, ok)

	popped, _ := h.Pop("c")
	assert.Equal(t, got, popped)
	assert.False(t, h.Remove("c"))

}
