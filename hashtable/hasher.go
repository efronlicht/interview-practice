package hashtable

type StringHasher interface {
	Hash(s string) int
}

type concreteHasher struct{}

func (c concreteHasher) Hash(s string) int {
	var sum int
	for i := range s {
		sum = 31*sum + int(s[i])
	}
	return sum
}

// NewHasher creates a new hasher
func NewHasher() StringHasher {
	return concreteHasher{}
}
