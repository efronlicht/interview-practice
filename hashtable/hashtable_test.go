package hashtable

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_hashmap(t *testing.T) {
	h := New()

	assert.Equal(t, h.Len(), 0)

	for b := byte('a'); b <= byte('z'); b++ {
		s := string(b)
		assert.False(t, h.Contains(s))

		h.Insert(string(b), []string{s, s})
		assert.True(t, h.Contains(s))

	}
	assert.Equal(t, h.Len(), 26)
	// replacement does not change length
	assert.Equal(t, h.Insert(string('c'), []string{"foo", "bar"}), []string{"c", "c"})
	assert.Equal(t, h.Len(), 26)

	got, ok := h.Get("c")

	assert.Equal(t, got, []string{"foo", "bar"})
	assert.True(t, ok)

	popped, _ := h.Pop("c")
	assert.Equal(t, got, popped)
	assert.False(t, h.Remove("c"))

}
