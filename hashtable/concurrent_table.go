package hashtable

import "sync"

type ConcurrentStringHashMap struct {
	m   *StringHashMap
	mux *sync.RWMutex
}

func NewConcurrentStringHashMap() ConcurrentStringHashMap {
	return ConcurrentStringHashMap{New(), new(sync.RWMutex)}
}

func (c ConcurrentStringHashMap) Get(k string) ([]string, bool) {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.m.Get(k)
}

// Contains checks the ConcurrentStringHashMap for the key k, returning true if it exists in the table.

func (c ConcurrentStringHashMap) Contains(k string) bool {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.m.Contains(k)
}

func (c ConcurrentStringHashMap) Len() int {
	c.mux.RLock()
	defer c.mux.RUnlock()
	return c.m.Len()
}

func (c ConcurrentStringHashMap) Insert(k string, v []string) []string {
	c.mux.Lock()
	defer c.mux.Unlock()
	return c.m.Insert(k, v)
}

func (c ConcurrentStringHashMap) Pop(k string) ([]string, bool) {
	c.mux.Lock()
	defer c.mux.Unlock()
	return c.m.Pop(k)
}

func (c ConcurrentStringHashMap) Remove(k string) bool {
	_, ok := c.Pop(k)
	return ok
}
