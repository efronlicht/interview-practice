package hashtable

type keyval struct {
	key string
	val []string
}

func get(slice []keyval, k string) ([]string, bool) {
	for _, kv := range slice {
		if kv.key == k {
			return kv.val, true
		}
	}
	return nil, false
}

func contains(slice []keyval, k string) bool {
	_, ok := get(slice, k)
	return ok
}
