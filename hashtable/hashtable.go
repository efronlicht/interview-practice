package hashtable

var _ Interface = New()
var _ Interface = NewConcurrentStringHashMap()

type Interface interface {
	Insert(k string, val []string) []string
	Get(k string) (val []string, ok bool)
	Contains(k string) bool
	Pop(k string) (val []string, ok bool)
	Remove(k string) bool
	Len() int
}

type StringHashMap struct {
	StringHasher
	table [][]keyval
	elems int
}

// New creates a new StringHashMap using the default StringHasher.
func New() *StringHashMap {
	return &StringHashMap{
		NewHasher(),
		make([][]keyval, 8),
		0,
	}
}

// Contains checks the StringHashMap for the key k, returning true if it exists in the table.
func (h *StringHashMap) Contains(s string) bool {
	for _, slice := range h.table {
		if contains(slice, s) {
			return true
		}
	}
	return false
}

func (h *StringHashMap) Get(k string) ([]string, bool) {
	i := h.idx(k)
	for _, kv := range h.table[i] {
		if k == kv.key {
			return kv.val, true
		}
	}
	return nil, false
}

// Pop an item from the hash table, returning the value if it exists. This does not preserve ordering!
func (h *StringHashMap) Pop(k string) ([]string, bool) {
	i := h.idx(k)
	for j, kv := range h.table[i] {
		if k == kv.key {
			h.table[i][j] = h.table[i][len(h.table[i])-1]
			h.table[i] = h.table[i][:len(h.table[i])-1]
			h.elems--
			return kv.val, true
		}
	}
	return nil, false

}

// Remove an item from the hash table, returning true if it was present
func (h *StringHashMap) Remove(k string) bool {
	_, ok := h.Pop(k)
	return ok
}

// Insert a key-val pair into the StringHashMap, returning the previous value, if any, on a successful insertion
func (h *StringHashMap) Insert(k string, v []string) []string {
	i := h.idx(k)
	for j, kv := range h.table[i] {
		if kv.key == k {
			h.table[i][j].val, v = v, h.table[i][j].val
			return v
		}
	}
	h.table[i] = append(h.table[i], keyval{k, v})

	h.elems++
	h.resizeIfNecessary()

	return nil
}

// Len returns the number of elements in the [StringHashMap].
func (h StringHashMap) Len() int {
	return h.elems
}

func (h *StringHashMap) shouldResize() bool {
	return h.elems > len(h.table)*2 // this is arbitrary

}

func (h *StringHashMap) resizeIfNecessary() {
	if !h.shouldResize() {
		return
	}
	elems := make([]keyval, 0, h.elems)
	for _, slice := range h.table {
		for _, s := range slice {
			elems = append(elems, s)
		}
	}

	// this is arbitrary; i'm sure there's some research on the right way to resize
	h.table = make([][]keyval, len(h.table)*2)

	for _, kv := range elems {
		i := h.idx(kv.key)
		h.table[i] = append(h.table[i], kv)
	}

}

func (h *StringHashMap) idx(k string) int {
	return h.Hash(k) % len(h.table)

}
