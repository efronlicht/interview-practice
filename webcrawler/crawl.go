package main

import (
	"fmt"
	"os"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

type Cache interface {
	Seen(url string) bool
	MarkSeen(url string)
}

type Results []Result

func (r Results) Len() int           { return len(r) }
func (r Results) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }
func (r Results) Less(i, j int) bool { return r[i].URL < r[j].URL }

type Result struct{ URL, Body string }

// Crawler crawls the web using it's Fetcher, storing it's result in it's Cache and sending results to out and errs to err
type Crawler struct {
	out  chan<- Result
	errs chan<- error
	Fetcher
	Cache
	wg *sync.WaitGroup
}

type cache struct {
	cache map[string]struct{}
	mux   *sync.RWMutex
}

func NewCache() Cache {
	return &cache{make(map[string]struct{}), new(sync.RWMutex)}
}

// Set the item in the cache, using a  lock to protect it.
func (c *cache) MarkSeen(url string) {
	c.mux.Lock()
	defer c.mux.Unlock()
	c.cache[url] = struct{}{}
}

// Get the item from the cache, using a read lock to protect it.
func (c *cache) Seen(url string) bool {
	c.mux.RLock()
	defer c.mux.RUnlock()
	_, ok := c.cache[url]
	return ok
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func (c *Crawler) Crawl(url string, depth int) {
	defer c.wg.Done()
	if depth <= 0 || c.Seen(url) {
		return
	}
	c.MarkSeen(url)

	body, links, err := fetcher.Fetch(url)
	if err != nil {
		c.errs <- err
		return
	}
	fmt.Fprintf(os.Stderr, "url: %v body: %v links: %v\n", url, body, links)

	c.out <- Result{url, body}

	for _, u := range links {
		if !c.Seen(u) {

			c.wg.Add(1)
			go c.Crawl(u, depth-1)

		}

	}
}
