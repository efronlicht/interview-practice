package main

import (
	"fmt"
	"os"
	"sync"
)

func Main(out chan Result, errs chan error) (results Results) {
	crawler := Crawler{
		out: out,

		errs:    errs,
		Fetcher: fetcher,
		Cache:   NewCache(),
		wg:      new(sync.WaitGroup),
	}
	crawler.wg.Add(1)
	crawler.Crawl("https://golang.org/", 4)
	go func() {
		crawler.wg.Wait()
		close(crawler.out)
		close(crawler.errs)
	}()

	return results
}
func main() {
	finished := make(chan struct{})
	out, errs := make(chan Result, 10), make(chan error, 10)
	go func() {
		for err := range errs {
			fmt.Fprintln(os.Stderr, err)
		}
		finished <- struct{}{}
	}()

	go func() {
		for res := range out {
			fmt.Printf("%s: %s\n", res.URL, res.Body)
		}
		finished <- struct{}{}
	}()

	Main(out, errs)
	<-finished
	<-finished
}
