package main

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

type ErrorNotFound string

func (e ErrorNotFound) Error() string {
	return "not found: " + string(e)
}
func Test(t *testing.T) {
	results, errs := make(chan Result, 10), make(chan error, 10)
	Main(results, errs)

	var gotURLs []string
	var gotErrs []error

	for err := range errs {
		gotErrs = append(gotErrs, err)
	}
	for res := range results {
		gotURLs = append(gotURLs, res.URL)
	}

	wantURLs := []string{"https://golang.org/", "https://golang.org/pkg/", "https://golang.org/pkg/fmt/", "https://golang.org/pkg/os/"}
	sort.Strings(gotURLs)
	sort.Strings(wantURLs)

	assert.Equal(t, wantURLs, gotURLs)

}
